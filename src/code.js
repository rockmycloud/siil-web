const API = "https://siil-app.rockmy.cloud/api/question"
let last_quest
let a_fields
let cBox
let q_lock

function get_question(){
    if(!q_lock) $.get(API, data => parse_question(data))
}

function parse_question(quest){
    if(last_quest == quest[0].question){
        get_question()
        return
    } 

    q_lock = true
    clear_answers()
    last_quest = quest[0].question
    $('#q_text').html(last_quest)
    $('#q_pic').css('visibility', 'hidden')

    if(quest[0].type == "sc") populate_answers(quest)
    if(quest[0].type == "tf") populate_tf(quest)
}

function populate_answers(quest){
    const answers = quest[0].wrong_answers.concat(quest[0].right_answer)
    let fields = [...Array(quest[0].wrong_answers.length+1).keys()]
    fields = fields.map(i => "answer_box" + i)
    a_fields = fields.slice();
    answers.forEach(e => {
        cBox = $('#' + fields.splice(Math.floor(Math.random() * fields.length),1))[0]
        $(cBox).html(e)
        $(cBox).css({'backgroundColor':'lightblue','visibility':'visible','border':'3px solid black'})
    })
}

function clear_answers(){
    if(a_fields) a_fields.forEach(e => $('#' + e).css('visibility', 'hidden'))
    a_fields = []
}

function populate_tf(quest){
    $("#answer_box0").css({'backgroundColor':'lightblue','visibility':'visible','border':'3px solid black'})
    $("#answer_box1").css({'backgroundColor':'lightblue','visibility':'visible','border':'3px solid black'})
    $("#answer_box0").html("True")
    $("#answer_box1").html("False")

    if(quest[0].right_answer == true) cBox = $("#answer_box0")[0]
    else cBox = $("#answer_box1")[0]
}

function lock_answer(ans){
    if(q_lock){
        q_lock = false
        $(ans).css('border', '3px solid orange')
        setTimeout(() => check_answer(ans), 700)
    }
}

function check_answer(ans){
    $('#q_pic').css('visibility', 'visible')
    if(ans.id == cBox.id) {
        $(ans).css('backgroundColor', 'lightgreen')
        if(parent.closeIFrame) parent.closeIFrame()
    }
    else $(ans).css('backgroundColor', 'lightcoral')
}