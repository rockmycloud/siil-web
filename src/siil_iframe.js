function prepareFrame() {
    var ifrm = document.createElement("iframe")
    ifrm.setAttribute("id", "myiframe")
    ifrm.setAttribute("src", "../index.html")
    ifrm.setAttribute("onload", "setupIFrame()")
    ifrm.style.position = "absolute"
    ifrm.style.width = "100%"
    ifrm.style.height = "100%"
    document.body.appendChild(ifrm)
}

function setupIFrame(){ 
    document.body.style.overflow="hidden"
    setTimeout(() => window.scrollTo(0, 0), 30)
}

function closeIFrame(){ 
    document.getElementById('myiframe').remove() 
    document.body.style.overflow="visible"
}

window.onload = prepareFrame();
